# -*- coding: utf-8 -*-

import logging

LogTypeInfo=0
LogTypeWarning=1
LogTypeError=2

def InitLog(FileName='program.log',Level=logging.DEBUG):
    logging.basicConfig(filename=FileName,level=Level)
    logging.info('='*100)

def ProcessLog(text,type=LogTypeInfo):
    print text
    if type==LogTypeError:
        logging.error(text)
    elif type==LogTypeWarning:
        logging.warning(text)
    else:
        logging.info(text)
