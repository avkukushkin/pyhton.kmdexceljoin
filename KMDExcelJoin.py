# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Alex
#
# Created:     21.08.2011
# Copyright:   (c) Alex 2011
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import alog
import re

from openpyxl import Workbook
from openpyxl.reader.excel import load_workbook


DataRowOffset=6
DataColOffset=3
NameAtrrRowOffset=4

class Column():
    name=""
    descr=""
    refObject=""
    refKey=""
    column=0
    def __init__(self, col, ws):
        self.col=col
        self.name=ws.cell(row=NameAtrrRowOffset, column=col).value
        self.descr=ws.cell(row=NameAtrrRowOffset-2, column=col).value
        ref=ws.cell(row=NameAtrrRowOffset-1, column=col).value
        self.column=col
        if ref:
            a=ref.split(".")
            if len(a)==2:
                self.refObject=a[0]
                self.refKey=a[1]
            else:
                alog.ProcessLog('#  Col %s has missing ref: %s' % (self.name,ref), alog.LogTypeWarning)
    def IsRef(self):
        if self.refObject and self.refKey:
            return True
        else:
            return False
                
class KmdObject():
    className=""
    sheetName=""
    cols={}
    indexedcol=[]
    IdMDMPIndex={}
    dataByColumn={}
    dataSourceByColumn=[]
    _dataRow0=0
    _dataCol0=0
    _dataNRow=0
    _dataNCol=0
    ws=None
    sourceDescr=""
    def Copy(self):
	n=KmdObject()
        n.className=self.className
        n.sheetName=self.sheetName
        n.cols=self.cols.copy()
        n.indexedcol=self.indexedcol
        n.IdMDMPIndex=self.IdMDMPIndex.copy()
        n.dataByColumn=self.dataByColumn.copy()
        n.dataSourceByColumn=self.dataSourceByColumn
        n._dataRow0=self._dataRow0
        n._dataCol0=self._dataCol0
        n._dataNRow=self._dataNRow
        n._dataNCol=self._dataNCol
        n.ws=None
        n.sourceDescr=self.sourceDescr
	return n
    def __init__(self):
        self.className=""
        self.sheetName=""
        self.cols={}
        self.indexedcol=[]
        self.IdMDMPIndex={}
        self.dataByColumn={}
        self.dataSourceByColumn=[]
        self._dataRow0=DataRowOffset
        self._dataCol0=DataColOffset
        self._dataNRow=0
        self._dataNCol=0
        self.ws=None
        self.sourceDescr=""
    def Load(self, className, ws, descr):
        self.className=className
        self.sheetName=ws.title
        self.ws=ws
        self._dataNRow=ws.max_row-DataRowOffset+1
        self._dataNCol=ws.max_column-self._dataCol0+1
        self.sourceDescr=descr
        alog.ProcessLog('#  Find class %s (%s). Recs=%s' %(self.className,self.sheetName,self._dataNRow))
        for col in xrange(self._dataCol0,self._dataCol0+self._dataNCol):
            col=Column(col,ws)
            if col.name:	# New column
                self.cols[col.name]=col
                if not(col.name in self.indexedcol):
                    self.indexedcol.append(col.name)
        alog.ProcessLog('#   Data coords(%s, %s, %s, %s)' %(self._dataRow0, self._dataCol0, self._dataNRow, self._dataNCol))
        pass
    @property
    def rowCount(self):
        return len(self.dataSourceByColumn)
    def ReadData(self):
        for colname in self.indexedcol:
            #alog.ProcessLog('#      colname=%s /%s' % (colname,self.indexedcol))
            col=self.cols[colname]
            prevData=self.dataByColumn[colname] if self.dataByColumn.has_key(colname) else [None]*len(self.dataSourceByColumn)
            newData=[c[0].value for c in self.ws.iter_rows(min_row=self._dataRow0, min_col=col.column, max_row=self._dataRow0+self._dataNRow-1, max_col=col.column)]
            #if self.className==u'CalcTempl' and colname==u'rules':
            #    alog.ProcessLog('#      Prev %s=%s' % (colname,str(prevData)))
            #    alog.ProcessLog('#      newData %s=%s' % (colname,str(newData)))
            self.dataByColumn[colname]=prevData+newData
            #if self.className==u'CalcTempl' and colname==u'rules':
            #    alog.ProcessLog('#      Total %s=%s' % (colname,str(self.dataByColumn[colname])))
        #if self.className==u'CalcTempl':
        #    alog.ProcessLog('#      Source before =%s' % (len(self.dataSourceByColumn)))
        self.dataSourceByColumn=self.dataSourceByColumn+[self.sourceDescr for c in xrange(len(newData))]
        #if self.className==u'CalcTempl':
        #    alog.ProcessLog('#      Source after =%s' % (len(self.dataSourceByColumn)))
        alog.ProcessLog('#   Processed read data class %s. Recs=%s' % (self.className, len(self.dataByColumn[colname])))

def GenerateSheetName(kmdObj):
    return kmdObj.sheetName.replace(' ','_').replace('-','_').replace('(','_').replace(')','_').replace('%','_').replace('#','_').replace('<','_').replace('>','_')

def WriteListLink(wslist, row, totalsBySource, kmdObj=None):
    if kmdObj==None:
        wslist.cell(row=row, column=1).value="№"
        wslist.cell(row=row, column=2).value="Лист"
        wslist.cell(row=row, column=3).value="Класс"
        wslist.cell(row=row, column=4).value="Записей"
        y=4
        for source in totalsBySource:
            y=y+1
            wslist.cell(row=row, column=y).value=source
    else:
        wslist.cell(row=row, column=1).value=row-1
        link=u'#%s!A1' % (GenerateSheetName(kmdObj))
        val=u'=HYPERLINK("{}", "{}")'.format(link,kmdObj.sheetName)
        wslist.cell(row=row, column=2).value=val
        wslist.cell(row=row, column=3).value=kmdObj.className
        wslist.cell(row=row, column=4).value=kmdObj.rowCount
        y=4
        for source in totalsBySource:
            y=y+1
            if totalsBySource[source].has_key(kmdObj.className):
                wslist.cell(row=row, column=y).value=totalsBySource[source][kmdObj.className]

def WriteSourceColumn(ws,kmdObj):
    ws.cell(row=4, column=1).value='Source'
    for y in xrange(kmdObj.rowCount):
        s=kmdObj.dataSourceByColumn[y]
        ws.cell(row=5+y, column=1).value=s   
    
def WriteColumn(ws,x,kmdObj,colname):
    col=kmdObj.cols[colname]
    ws.cell(row=1, column=1).value=kmdObj.className
    ws.cell(row=1, column=x).value=col.descr
    ws.cell(row=2, column=x).value=col.refObject+"."+col.refKey if col.refObject else ""
    ws.cell(row=4, column=x).value=col.name
    #print "    ",kmdObj.className.encode('cp866'),colname.encode('cp866'),kmdObj.rowCount,len(kmdObj.dataByColumn[col.name])
    for y in xrange(kmdObj.rowCount):
        ws.cell(row=5+y, column=x).value=kmdObj.dataByColumn[col.name][y]
        
class EkpitOutDataSet():
    kmdobject={}
    filename=""
    sourcefilename=""
    wb=None
    funcFilterSheetName=None
    def __ReadStructAndData__(self,):
        alog.ProcessLog('# Read structure...')
        for sheet_name in self.wb.get_sheet_names():
	    if not(self.funcFilterSheetName is None):
	        filterResult=self.funcFilterSheetName(sheet_name)
	        if not(filterResult.isFiltred):
	            alog.ProcessLog('#  Sheet %s skip by filter' % (sheet_name),alog.LogTypeWarning)
	            continue
            ws=self.wb[sheet_name]
            s=ws.cell(row=1, column=1).value
            if s=="1:{ListType:ClassCode}":
                s=ws.cell(row=1, column=2).value
                tmp=re.findall(u'{10:(\w*)',s)
                if len(tmp):
                    #Read structure
                    classname=tmp[0]
                    if self.kmdobject.has_key(classname):
                        alog.ProcessLog('#  Sheet %s - dublicate class %s' % (sheet_name, classname),alog.LogTypeWarning)
			cl=self.kmdobject[classname]
                        cl.Load(classname,ws,self.sourcefilename)
                    else:
                        cl=KmdObject()
                        cl.Load(classname,ws,self.sourcefilename)
                        self.kmdobject[classname]=cl
                    #ReadData
                    cl.ReadData()
                else:
                    alog.ProcessLog('#  Skip list %s (class %s)' % (sheet_name, s),alog.LogTypeWarning)
    def __init__(self, filename, funcFilterSheetName=None):
       	alog.ProcessLog('#Start process file %s.' % (filename))
        self.filename=filename
	self.funcFilterSheetName=funcFilterSheetName
	if self.funcFilterSheetName is None:
	    self.sourcefilename=self.filename
	else:
	    filterResult=self.funcFilterSheetName('')
	    self.sourcefilename=filterResult.ClassPrefix+self.filename
	    alog.ProcessLog('# Filter func %s. Sourcefilename=%s' % (self.funcFilterSheetName.__name__, self.sourcefilename))
        self.kmdobject={}
        self.wb = load_workbook(self.filename, data_only=True)
        self.__ReadStructAndData__()
        alog.ProcessLog('#End process file. Class count %s' % (len(self.kmdobject)))
    def Merge(self, self2):
        # Merge exists class
        alog.ProcessLog('#Start merge file %s <- %s' % (self.filename,self2.filename))
	#print self.kmdobject
        for className in self.kmdobject:
            if self2.kmdobject.has_key(className):
                kmdObj=self.kmdobject[className]
                kmdObj2=self2.kmdobject[className]
		#print '->',className,kmdObj,kmdObj2 
                kmdObj.dataSourceByColumn=kmdObj.dataSourceByColumn+kmdObj2.dataSourceByColumn
                #Exists column
                for colname in kmdObj.indexedcol:
                    if kmdObj2.cols.has_key(colname):
                        #kmdObj.IdMDMPIndex.update(kmdObj2.IdMDMPIndex)
                        kmdObj.dataByColumn[colname]=kmdObj.dataByColumn[colname]+kmdObj2.dataByColumn[colname]
                    else:
                        dummy=[None for c in xrange(kmdObj.rowCount)]
                        #kmdObj.IdMDMPIndex.update(dummy)
                        kmdObj.dataByColumn[colname]=kmdObj.dataByColumn[colname]+dummy
        # Add new class
        for className in self2.kmdobject:
            if not(self.kmdobject.has_key(className)):
                kmdObj2=self2.kmdobject[className]
                kmdObj=kmdObj2.Copy()
                self.kmdobject[kmdObj.className]=kmdObj
        pass
    def SaveAs(self, newfilename):
        alog.ProcessLog('#Start SaveAs to %s' % (newfilename))
        wb2=Workbook()
        wslist=wb2.active
        #List header
        kmlListPos=0
        # CalcTotalsBySource
	TotalsBySource={}
        for className in self.kmdobject:
            kmdObj=self.kmdobject[className]
            for y in xrange(kmdObj.rowCount):
                source=kmdObj.dataSourceByColumn[y]
		if not(TotalsBySource.has_key(source)):
                    TotalsBySource[source]={}
                if not(TotalsBySource[source].has_key(className)):
                    TotalsBySource[source][className]=0
                TotalsBySource[source][className]=TotalsBySource[source][className]+1
        #        
        WriteListLink(wslist,1,TotalsBySource)
        #
        for className in self.kmdobject:
            kmdObj=self.kmdobject[className]
            alog.ProcessLog('# Process sheet %s' % (kmdObj.sheetName))
            WriteListLink(wslist,2+kmlListPos,TotalsBySource,kmdObj)
            ws=wb2.create_sheet(GenerateSheetName(kmdObj))
            WriteSourceColumn(ws,kmdObj)
            x=2
            for colname in kmdObj.indexedcol:
                col=kmdObj.cols[colname]
                WriteColumn(ws,x,kmdObj,colname)
                if col.refObject:
                   if self.kmdobject.has_key(col.refObject):
                        kmdObjRef=self.kmdobject[col.refObject]
                        #ws.cell(row=4, column=x).value="name"
                        #CopyColumn(ws,x,kmdObj,)
                   else:
                        pass
                        #alog.ProcessLog('#  column %s has unknown link to %s.%s' % (colname,col.refObject,col.refKey),alog.LogTypeWarning)
                x=x+1
            kmlListPos=kmlListPos+1
        wb2.save(newfilename)
        alog.ProcessLog('#End process file')

def FilterSheetNameStart38(sheetname):
	ret=FilterResult()
	ret.ClassPrefix="38."
	if sheetname[:3]=="38.":
	    ret.isFiltred=True
	return ret 

def FilterSheetNameStart24(sheetname):
	ret=FilterResult()
	ret.ClassPrefix="24."
	if sheetname[:3]=="24.":
	    ret.isFiltred=True
	return ret 

def FilterSheetNameClear(sheetname):
	ret=FilterResult()
	if len(re.findall('^\d\d\..',sheetname))==0:
	    ret.isFiltred=True
	return ret 

class FilterResult():
	isFiltred=False
	ClassPrefix=""

def main():
	alog.InitLog('KMDExcelJoin.log')


	d   =EkpitOutDataSet(u"Справочники КМД 3.0(базовый).xlsx")
	d010=EkpitOutDataSet(u"Справочники КМД 3.0.xlsx",FilterSheetNameClear)
	d020=EkpitOutDataSet(u"Справочники КМД 3.0.xlsx",FilterSheetNameStart24)
	d030=EkpitOutDataSet(u"Справочники КМД 3.0.xlsx",FilterSheetNameStart38)

	d.Merge(d010)
	d.Merge(d020)
	d.Merge(d030)
	d.SaveAs("out.xls")
	pass


if __name__ == '__main__':
    main()